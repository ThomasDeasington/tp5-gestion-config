vagrant ssh

## DANS LA VM
sudo dnf install openscap
sudo dnf install openscap-scanner
sudo dnf install openscap-utils
sudo dnf install scap-security-guide

oscap info /usr/share/xml/scap/ssg/content/ssg-rl9-ds.xml
sudo oscap xccdf eval --report report.html --profile xccdf_org.ssgproject.content_profile_anssi_bp28_intermediary /usr/share/xml/scap/ssg/content/ssg-rl9-ds.xml

## EN DEHORS DE LA VM
vagrant ssh-config > .ssh-config
ssh -F .ssh-config default
scp -F .ssh-config default:/home/vagrant/report.html .

## DANS LA VM
oscap xccdf eval --profile xccdf_org.ssgproject.content_profile_anssi_bp28_intermediary --results report.xml /usr/share/xml/scap/ssg/content/ssg-rl9-ds.xml
oscap xccdf generate fix --fix-type ansible --profile xccdf_org.ssgproject.content_profile_anssi_bp28_intermediary --output remediation.yml report.xml

## EN DEHORS DE LA VM
scp -F .ssh-config default:/home/vagrant/remediation.yml ./playbooks
ansible-playbook playbooks/remediation.yml


ssh -i ssh-key vagrant@6.tcp.eu.ngrok.io -p 15919